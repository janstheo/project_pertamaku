package com.renseki.app.projectpertamaku.webservices

data class Post(
        val userId: Int,
        val id: Int,
        val title: String,
        val body: String
)